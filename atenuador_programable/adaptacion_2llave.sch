<QucsStudio Schematic 4.3.1>
<Properties>
View=-3387,-2628,1797,1513,0.584305,1056,1537
Grid=10,10,1
DataSet=*.dat
DataDisplay=*.dpl
OpenDisplay=1
showFrame=0
FrameText0=Title
FrameText1=Drawn By:
FrameText2=Date:
FrameText3=Revision:
</Properties>
<Symbol>
</Symbol>
<Components>
.SP SP1 1 -300 80 0 63 0 0 "lin" 1 "10 MHz" 1 "3 GHz" 1 "100" 1 "no" 0 "1" 0 "2" 0 "none" 0
GND * 1 480 30 0 0 0 0
GND * 1 -10 60 0 0 0 0
SPfile X2 1 220 -80 -2939 -1957 0 0 "C:/users/ignacioscharn/Documents/ektocomms/attenuadores_rf/PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 1 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
GND * 1 220 -30 0 0 0 0
C C1 1 -110 270 17 -26 0 1 "0.892 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
GND * 1 -110 300 0 0 0 0
L L1 1 60 240 -26 10 0 0 "2.05044 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
GND * 1 190 320 0 0 0 0
L L2 1 310 240 -26 10 0 0 "0 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
SPfile X1 1 190 240 -2939 -1957 0 0 "C:/users/ignacioscharn/Documents/ektocomms/attenuadores_rf/PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 1 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
SPfile X3 1 450 240 -3837 -2868 0 2 "C:/users/ignacioscharn/Documents/ektocomms/attenuadores_rf/PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 1 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
GND * 1 450 190 0 0 0 2
L L3 1 540 240 -26 10 0 0 "2.05044 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
Pac P2 1 750 290 18 -26 0 0 "2" 1 "50 Ω" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "SUBCLICK" 0
GND * 1 750 350 0 0 0 0
C C2 1 620 270 17 -26 0 1 "0.916976 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
GND * 1 620 300 0 0 0 0
Pac P1 1 -200 320 18 -26 0 0 "1" 1 "50 Ω" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "SUBCLICK" 0
GND * 1 -200 380 0 0 0 0
Pac P3 1 -10 0 18 -26 0 0 "3" 1 "50 Ω" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "SUBCLICK" 0
Pac P4 1 480 0 18 -26 0 0 "4" 1 "50 Ω" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "SUBCLICK" 0
</Components>
<Wires>
-10 30 -10 60 "" 0 0 0 ""
480 -80 480 -30 "" 0 0 0 ""
220 -50 220 -30 "" 0 0 0 ""
-10 -80 -10 -30 "" 0 0 0 ""
-10 -80 190 -80 "" 0 0 0 ""
250 -80 480 -80 "" 0 0 0 ""
-110 240 30 240 "" 0 0 0 ""
90 240 160 240 "" 0 0 0 ""
190 270 190 320 "" 0 0 0 ""
220 240 280 240 "" 0 0 0 ""
340 240 420 240 "" 0 0 0 ""
480 240 510 240 "" 0 0 0 ""
450 190 450 210 "" 0 0 0 ""
570 240 620 240 "" 0 0 0 ""
750 240 750 260 "" 0 0 0 ""
750 320 750 350 "" 0 0 0 ""
620 240 750 240 "" 0 0 0 ""
-200 240 -110 240 "" 0 0 0 ""
-200 240 -200 290 "" 0 0 0 ""
-200 340 -200 350 "" 0 0 0 ""
-200 350 -200 380 "" 0 0 0 ""
</Wires>
<Diagrams>
<Rect -371 786 386 296 31 #c0c0c0 1 00 1 0 5e+08 3e+09 1 -40 10 -7.94351 1 0 0 0 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"dB(S[1,1])" "" #0000ff 0 3 0 0 0 0 "">
	<"dB(S[3,3])" "" #ff0000 0 3 0 0 0 0 "">
</Rect>
<Smith 119 823 363 363 31 #c0c0c0 1 00 1 0 1 1 1 0 4 1 1 0 4 1 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"S[1,1]" "" #0000ff 0 3 0 0 0 0 "">
	<"S[3,3]" "" #ff0000 0 3 0 0 0 0 "">
</Smith>
<Rect -404 1275 477 405 31 #c0c0c0 1 00 1 0 5e+08 3e+09 1 -40 5 -7.10875 1 0 0 0 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"dB(S[2,2])" "" #0000ff 0 3 0 0 0 0 "">
	<"dB(S[4,4])" "" #ff0000 0 3 0 0 0 0 "">
</Rect>
<Smith 128 1294 405 405 31 #c0c0c0 1 00 1 0 1 1 1 0 4 1 1 0 4 1 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"S[2,2]" "" #0000ff 0 3 0 0 0 0 "">
	<"S[4,4]" "" #ff0000 0 3 0 0 0 0 "">
</Smith>
</Diagrams>
<Paintings>
Rectangle -480 -130 1430 1520 #000000 2 1 #c0c0c0 1 0
</Paintings>
