<QucsStudio Schematic 4.3.1>
<Properties>
View=-2759,-1937,1797,1164,0.690542,1583,1061
Grid=10,10,1
DataSet=*.dat
DataDisplay=*.dpl
OpenDisplay=1
showFrame=0
FrameText0=Title
FrameText1=Drawn By:
FrameText2=Date:
FrameText3=Revision:
</Properties>
<Symbol>
</Symbol>
<Components>
.SP SP1 1 -300 80 0 63 0 0 "lin" 1 "10 MHz" 1 "3 GHz" 1 "100" 1 "no" 0 "1" 0 "2" 0 "none" 0
Pac P3 1 480 0 18 -26 0 0 "3" 1 "50 Ω" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "SUBCLICK" 0
GND * 1 480 30 0 0 0 0
Pac P4 1 -10 0 18 -26 0 0 "4" 1 "50 Ω" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "SUBCLICK" 0
GND * 1 -10 60 0 0 0 0
SPfile X2 1 220 -80 -2939 -1957 0 0 "C:/users/ignacioscharn/Documents/ektocomms/attenuadores_rf/PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 1 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
GND * 1 220 -30 0 0 0 0
C C1 1 20 280 17 -26 0 1 "0.892 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
GND * 1 20 310 0 0 0 0
Pac P1 1 -50 330 18 -26 0 0 "1" 1 "50 Ω" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "SUBCLICK" 0
GND * 1 -50 390 0 0 0 0
L L1 1 190 250 -26 10 0 0 "2.179 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
SPfile X1 1 320 250 -2939 -1957 0 0 "C:/users/ignacioscharn/Documents/ektocomms/attenuadores_rf/PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 1 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
GND * 1 320 330 0 0 0 0
Pac P2 1 550 310 18 -26 0 0 "2" 1 "50 Ω" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "SUBCLICK" 0
GND * 1 550 390 0 0 0 0
L L2 1 440 250 -26 10 0 0 "0.470664 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
</Components>
<Wires>
-10 30 -10 60 "" 0 0 0 ""
480 -80 480 -30 "" 0 0 0 ""
220 -50 220 -30 "" 0 0 0 ""
-10 -80 -10 -30 "" 0 0 0 ""
-10 -80 190 -80 "" 0 0 0 ""
-50 250 20 250 "" 0 0 0 ""
-50 250 -50 300 "" 0 0 0 ""
-50 360 -50 390 "" 0 0 0 ""
20 250 160 250 "" 0 0 0 ""
220 250 290 250 "" 0 0 0 ""
320 280 320 330 "" 0 0 0 ""
350 250 410 250 "" 0 0 0 ""
550 250 550 280 "" 0 0 0 ""
550 340 550 390 "" 0 0 0 ""
250 -80 480 -80 "" 0 0 0 ""
470 250 550 250 "" 0 0 0 ""
</Wires>
<Diagrams>
<Rect -371 786 386 296 31 #c0c0c0 1 00 1 0 5e+08 3e+09 1 -40 10 -8.05755 1 0 0 0 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"dB(S[1,1])" "" #0000ff 0 3 0 0 0 0 "">
	<"dB(S[3,3])" "" #ff0000 0 3 0 0 0 0 "">
</Rect>
<Smith 119 823 363 363 31 #c0c0c0 1 00 1 0 1 1 1 0 4 1 1 0 4 1 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"S[1,1]" "" #0000ff 0 3 0 0 0 0 "">
	<"S[3,3]" "" #ff0000 0 3 0 0 0 0 "">
</Smith>
<Rect -404 1275 477 405 31 #c0c0c0 1 00 1 0 5e+08 3e+09 1 -33.3268 5 -8.49653 1 0 0 0 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"dB(S[2,2])" "" #0000ff 0 3 0 0 0 0 "">
	<"dB(S[4,4])" "" #ff0000 0 3 0 0 0 0 "">
</Rect>
<Smith 128 1294 405 405 31 #c0c0c0 1 00 1 0 1 1 1 0 4 1 1 0 4 1 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"S[2,2]" "" #0000ff 0 3 0 0 0 0 "">
	  <Mkr 2.9396e+09 282 -234 3 1 0 0 1 50>
	  <Mkr 3e+09 282 -234 3 1 0 0 1 50>
	<"S[4,4]" "" #ff0000 0 3 0 0 0 0 "">
</Smith>
</Diagrams>
<Paintings>
</Paintings>
