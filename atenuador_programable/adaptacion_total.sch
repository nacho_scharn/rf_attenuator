<QucsStudio Schematic 4.3.1>
<Properties>
View=-3487,-3088,5427,1097,0.584308,1305,1392
Grid=10,10,1
DataSet=*.dat
DataDisplay=*.dpl
OpenDisplay=1
showFrame=0
FrameText0=Title
FrameText1=Drawn By:
FrameText2=Date:
FrameText3=Revision:
</Properties>
<Symbol>
</Symbol>
<Components>
.SP SP1 1 -350 -250 0 63 0 0 "lin" 1 "10 MHz" 1 "3 GHz" 1 "100" 1 "no" 0 "1" 0 "2" 0 "none" 0
GND * 1 400 -160 0 0 0 0
GND * 1 700 -140 0 0 0 0
SPfile X1 1 700 -220 -2939 -1957 0 0 "C:/users/ignacioscharn/Documents/ektocomms/attenuadores_rf/PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 1 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
SPfile X3 1 960 -220 -3837 -2868 0 2 "C:/users/ignacioscharn/Documents/ektocomms/attenuadores_rf/PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 1 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
GND * 1 1130 -160 0 0 0 0
Pac P1 1 310 -140 18 -26 0 0 "1" 1 "50 Ω" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "SUBCLICK" 0
GND * 1 310 -80 0 0 0 0
GND * 1 960 -270 0 0 0 2
GND * 1 1130 40 0 0 1 2
GND * 1 830 60 0 0 1 2
SPfile X4 1 830 -20 1984 -1957 1 2 "C:/users/ignacioscharn/Documents/ektocomms/attenuadores_rf/PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 1 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
SPfile X5 1 650 -20 2882 -2868 1 0 "C:/users/ignacioscharn/Documents/ektocomms/attenuadores_rf/PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 1 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
GND * 1 650 -70 0 0 1 0
GND * 1 480 40 0 0 1 2
GND * 1 710 230 0 0 0 0
SPfile X6 1 710 150 -2939 -1957 0 0 "C:/users/ignacioscharn/Documents/ektocomms/attenuadores_rf/PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 1 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
SPfile X7 1 970 150 -3837 -2868 0 2 "C:/users/ignacioscharn/Documents/ektocomms/attenuadores_rf/PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 1 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
GND * 1 1140 210 0 0 0 0
GND * 1 970 100 0 0 0 2
GND * 1 1140 380 0 0 1 2
GND * 1 840 400 0 0 1 2
SPfile X8 1 840 320 1984 -1957 1 2 "C:/users/ignacioscharn/Documents/ektocomms/attenuadores_rf/PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 1 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
SPfile X9 1 660 320 2882 -2868 1 0 "C:/users/ignacioscharn/Documents/ektocomms/attenuadores_rf/PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 1 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
GND * 1 660 270 0 0 1 0
GND * 1 490 380 0 0 1 2
GND * 1 480 570 0 0 0 0
GND * 1 780 590 0 0 0 0
SPfile X10 1 780 510 -2939 -1957 0 0 "C:/users/ignacioscharn/Documents/ektocomms/attenuadores_rf/PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 1 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
SPfile X11 1 1040 510 -3837 -2868 0 2 "C:/users/ignacioscharn/Documents/ektocomms/attenuadores_rf/PE4259_de_embed_S2P/PE4259_RFC_RF1_de_embed.s2p" 1 "2" 1 "polar" 0 "linear" 0 "short" 0 "none" 0 "block" 0 "SOT23" 0
GND * 1 1210 570 0 0 0 0
GND * 1 1040 460 0 0 0 2
Pac P2 1 1410 550 -80 -26 1 2 "2" 1 "50 Ω" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0 "SUBCLICK" 0
GND * 1 1410 600 0 0 1 2
GND * 1 490 220 0 0 0 0
C C9 1 480 540 17 -26 0 1 "1 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
C C10 1 1210 540 17 -26 0 1 "1 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
C C1 1 400 -190 17 -26 0 1 "1 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
C C2 1 1130 -190 17 -26 0 1 "1 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
C C4 1 480 0 -30 -79 1 1 "1 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
C C3 1 1130 10 20 -11 1 1 "1 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
C C5 1 490 180 -85 -13 0 1 "1 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
C C6 1 1140 180 17 -26 0 1 "1 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
C C8 1 490 350 9 4 1 1 "1 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
C C7 1 1140 350 15 -16 1 1 "1 pF" 1 "0" 0 "" 0 "neutral" 0 "SMD0603" 0
L L1 1 570 -220 -26 10 0 0 "3.3 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
L L3 1 1050 -220 -26 10 0 0 "3.3 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
L L6 1 560 -20 -23 12 1 2 "3.3 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
L L4 1 960 -20 -25 12 1 2 "3.3 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
L L7 1 580 150 -31 -56 0 0 "3.3 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
L L9 1 1060 150 -26 10 0 0 "3.3 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
L L12 1 570 320 -30 -50 1 2 "3.3 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
L L10 1 970 320 -40 -50 1 2 "3.3 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
L L13 1 650 510 -32 -53 0 0 "3.3 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
L L15 1 1130 510 -37 -55 0 0 "3.3 nH" 1 "0" 0 "" 0 "SELF-WE-PD3S" 0
</Components>
<Wires>
600 -220 670 -220 "" 0 0 0 ""
700 -190 700 -140 "" 0 0 0 ""
990 -220 1020 -220 "" 0 0 0 ""
1080 -220 1130 -220 "" 0 0 0 ""
310 -220 310 -170 "" 0 0 0 ""
310 -120 310 -110 "" 0 0 0 ""
310 -110 310 -80 "" 0 0 0 ""
960 -270 960 -250 "" 0 0 0 ""
990 -20 1130 -20 "" 0 0 0 ""
860 -20 930 -20 "" 0 0 0 ""
830 10 830 60 "" 0 0 0 ""
1280 -220 1280 -20 "" 0 0 0 ""
590 -20 620 -20 "" 0 0 0 ""
650 -70 650 -50 "" 0 0 0 ""
610 150 680 150 "" 0 0 0 ""
710 180 710 230 "" 0 0 0 ""
1000 150 1030 150 "" 0 0 0 ""
1090 150 1140 150 "" 0 0 0 ""
970 100 970 120 "" 0 0 0 ""
400 -20 480 -20 "" 0 0 0 ""
400 -20 400 150 "" 0 0 0 ""
400 150 490 150 "" 0 0 0 ""
1000 320 1140 320 "" 0 0 0 ""
870 320 940 320 "" 0 0 0 ""
840 350 840 400 "" 0 0 0 ""
600 320 630 320 "" 0 0 0 ""
660 270 660 290 "" 0 0 0 ""
1290 150 1290 320 "" 0 0 0 ""
680 510 750 510 "" 0 0 0 ""
780 540 780 590 "" 0 0 0 ""
1070 510 1100 510 "" 0 0 0 ""
1160 510 1210 510 "" 0 0 0 ""
1040 460 1040 480 "" 0 0 0 ""
440 320 490 320 "" 0 0 0 ""
440 320 440 510 "" 0 0 0 ""
440 510 480 510 "" 0 0 0 ""
730 -220 930 -220 "" 0 0 0 ""
680 -20 800 -20 "" 0 0 0 ""
740 150 940 150 "" 0 0 0 ""
690 320 810 320 "" 0 0 0 ""
810 510 1010 510 "" 0 0 0 ""
1410 510 1410 520 "" 0 0 0 ""
1410 580 1410 600 "" 0 0 0 ""
310 -220 400 -220 "" 0 0 0 ""
480 -20 530 -20 "" 0 0 0 ""
480 -30 480 -20 "" 0 0 0 ""
480 30 480 40 "" 0 0 0 ""
490 210 490 220 "" 0 0 0 ""
480 510 620 510 "" 0 0 0 ""
1210 510 1410 510 "" 0 0 0 ""
400 -220 540 -220 "" 0 0 0 ""
1130 -220 1280 -220 "" 0 0 0 ""
1130 -20 1280 -20 "" 0 0 0 ""
490 150 550 150 "" 0 0 0 ""
1140 150 1290 150 "" 0 0 0 ""
490 320 540 320 "" 0 0 0 ""
1140 320 1290 320 "" 0 0 0 ""
</Wires>
<Diagrams>
<Rect -686 314 419 362 31 #c0c0c0 1 00 1 0 5e+08 3e+09 1 -31.5026 5 1.65465 1 0 0 0 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"dB(S[1,1])" "" #ff00ff 0 3 0 0 0 0 "">
</Rect>
<Rect -714 815 477 405 31 #c0c0c0 1 00 1 0 5e+08 3e+09 1 -35 5 3.05737 1 0 0 0 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"dB(S[2,2])" "" #0000ff 0 3 0 0 0 0 "">
</Rect>
<Smith -112 814 405 405 31 #c0c0c0 1 00 1 0 1 1 1 0 4 1 1 0 4 1 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"S[2,2]" "" #0000ff 0 3 0 0 0 0 "">
</Smith>
<Smith -101 323 363 363 31 #c0c0c0 1 00 1 0 1 1 1 0 4 1 1 0 4 1 315 0 225 "" "" "">
	<Legend 10 -100 0>
	<"S[1,1]" "" #0000ff 0 3 0 0 0 0 "">
</Smith>
</Diagrams>
<Paintings>
Rectangle -770 -480 2420 1350 #000000 2 1 #c0c0c0 1 0
</Paintings>
